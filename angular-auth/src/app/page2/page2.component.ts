import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {
  private message:string = '';

  constructor(private authService:AuthService) { }

  ngOnInit() {
  }

  logoutUser() {
    this.authService.logOut().subscribe(() => {
      this.message = 'Loged Out'
    });
  }

}
