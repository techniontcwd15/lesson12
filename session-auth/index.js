const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session')
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const app = express();

mongoose.connect('mongodb://localhost/test', { useMongoClient: true, promiseLibrary: global.Promise });
const User = mongoose.model('User', { name: String, password: String });
const salt = bcrypt.genSaltSync(10);



function checkAuth(req, res, next) {
  console.log('checkAuth ' + req.url);
  console.log(req.session, req.session.authenticated);
  if (req.url !== '/login' && (!req.session || !req.session.authenticated)) {
    res.sendStatus(403);
    return;
  }

  next();
}

app.use(cors({credentials: true, origin: 'http://localhost:4200'}));
app.use(cookieParser());
app.use(cookieSession({ secret: 'shhhh' }));
app.use(bodyParser());

app.use(checkAuth);

app.get('/logedIn', (req, res) => {
  res.json({ok: req.session.authenticated ? true : false});
});

app.post('/login', (req, res) => {
  User.find({
    name: req.body.username
  }).exec()
  .then(([user]) => {
    if(bcrypt.compareSync(req.body.password, user.password)) {
        req.session.authenticated = true;
        req.session.username = user.name;
        res.json({ok: true});
    } else {
      res.json({ok: false})
    }
  }).catch(err => {
    res.json({ok: false})
  })
});

app.get('/logout', (req, res) => {
  delete req.session.authenticated;
  res.sendStatus(204);
});

app.listen(8081);
